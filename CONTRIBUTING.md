# Contributing
We are open to, and grateful for, any contributions made by the community

## Reporting Issues and Asking Questions
Before opening an issue, please search the [issue tracker](https://bitbucket.org/pinturic/crema/issues) to make sure your issue hasn't already been reported.

## Development

Visit the [Issue tracker](https://bitbucket.org/pinturic/crema/issues) to find a list of open issues that need attention.

Fork, then clone the repo:
``` 
git clone https://bitbucket.org/your-username/crema.git
```

###New Features

Please open an issue with a proposal for a new feature or refactoring. When the issue is worth developing effort a new pull request can be opened.

## Submitting Changes

* Open a new issue in the [Issue tracker](https://bitbucket.org/pinturic/crema/issues).
* Fork the repo.
* Create a new feature branch based off the `master` branch.
* Submit a pull request, referencing any issues it addresses.

Thank you for contributing!