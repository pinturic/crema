const path = require('path');
const Dotenv = require('dotenv-webpack');

const srcFolder = path.join(__dirname, '../src');
console.log(`Src folder is: ${srcFolder}`);
const nodeModulesResolvedPath = path.resolve(__dirname, '../node_modules');
console.log(`NodeModulesResolvedPath is: ${nodeModulesResolvedPath}`);

module.exports = {
  mode: 'production',
  entry: {
    main: './src/app'
  },
  output: {
    path: path.join(__dirname, '../devices/build/ios/www'),
    filename: '[name].js',
    chunkFilename: '[id].js',
    publicPath: './',
    sourceMapFilename: 'debugging/[file].map',
    libraryTarget: undefined,
    pathinfo: true
  },
  target: 'web',
  plugins: [
    new Dotenv({
      path: './config/ios.env', // if not simply .env
      safe: false // lets load the .env.example file as well
    })
  ],
  module: {
    rules: [{
      test: /\.jsx?$/,
      use: [{
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env',
            {
              plugins: [
                '@babel/plugin-proposal-class-properties'
              ]
            }]
        }
      }],
      include: [srcFolder]
    },
    {
      test: /\.html$/,
      use: [{
        loader: 'file-loader?name=[name].[ext]'
      }]
    },
    {
      test: /\.(png|jpg|jpeg|gif)$/,
      use: [{
        loader: 'url-loader?limit=10000&name=[name].[ext]'
      }]
    },
    {
      test: /\.css$/,
      exclude: /\.useable\.css$/,
      use: ['style-loader', 'css-loader']
    },
    {
      test: /\.useable\.css$/,
      use: ['style-loader/useable', 'css-loader']
    },
    {
      test: /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
      use: [{
        loader: 'url-loader'
      }]
    }
    ]
  },
  externals: {

  },
  resolve: {
    modules: [
      'node_modules',
      srcFolder,
      nodeModulesResolvedPath
    ],
    extensions: ['.js', '.jsx'],
  }
};
