#!/usr/bin/env node
/*
	This script builds the cordova application for android or ios
*/
const execSync = require('child_process').execSync;
const cordovaTasks = require('./cordova-tasks');

const appName = `${process.argv[1].match(/(?:.*[\/\\])?(.*)$/)[1]}`;

const opts = require('node-getopt').create([
  ['a', 'add=ARG', 'Add a plugin'],
  ['l', 'list', 'List plugins'],
  ['r', 'rm=ARG', 'Remove a plugin'],
  ['p', 'platform=ARG', 'One of android or ios'],
  ['h', 'help', 'display this help']
])
  .bindHelp(`Usage: node ${appName} [PROD] [TARGET] [PLAT] [-- POPTS]\n` +
  'Manage plugins for cordova application\n' +
  'ADD:   --add=<plugin_name>\n' +
  'REMOVE: --remove=<plugin_name>\n' +
  'LIST: --list\n' +
  'PLAT:  --platform=[android|ios]\n' +
  '\n' +
  '[[OPTIONS]]\n' +
  'Examples:\n' +
  `	node ${appName} -p android -l \n` +
  `	node ${appName} --platform=ios -a cordova-plugin-console\n` +
 `  node ${appName} --platform=ios -r cordova-plugin-console\n`)
  .parseSystem();

// normalize opt
const options = opts.options;
if (!options.platform) {
  options.platform = 'android';
}

console.info(`Executing for: ${
  options.platform
}${(options.add && ' add ') ||
  (options.rm && ' rm ') ||
  (options.list && ' list ')}`);

const listPlugins = (platform) => {
  console.log('Available plugins:');
  cordovaTasks.listPlugins(platform).then((plugins) => {
    for (const plugin of plugins) {
      console.log(plugin);
    }
  });
};

if (options.list) {
  listPlugins(options.platform);
} else if (options.rm) {
  cordovaTasks.rmPlugin(
    options.platform,
    options.rm
  )
    .then(() => listPlugins(options.platform));
} else if (options.add) {
  cordovaTasks.addPlugin(
    options.platform,
    options.add
  )
    .then(() => listPlugins(options.platform));
}

