#!/usr/bin/env node
/*
  This script builds the production web application
*/

const mkdirpSync = require('./utils.js').mkdirpSync;
const copyAssets = require('./utils.js').copyAssets;
const execSync = require('child_process').execSync;

const outputFolder = 'output';
const platform = 'web';
mkdirpSync(`${outputFolder}/${platform}`);

copyAssets(
  platform,
  process.cwd(),
  `${outputFolder}/${platform}`,
  () => {
    `Copied files to: ${platform}`;
  }
);

execSync('NODE_ENV=production webpack  --progress --profile --config scripts/webpack.build.config.js --mode production', { stdio: [0, 1, 2] });
