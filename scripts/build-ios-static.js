#!/usr/bin/env node
/*
  This script builds the static files for ios
*/

const mkdirpSync = require('./utils.js').mkdirpSync;
const copyAssets = require('./utils.js').copyAssets;
const execSync = require('child_process').execSync;

const outputFolder = 'devices/build';
const platform = 'ios';
mkdirpSync(`${outputFolder}/${platform}`);

copyAssets(
  platform,
  process.cwd(),
  `${outputFolder}/${platform}/www`,
  () => {
    `Copied files to: ${platform}`;
  }
);

console.log('Build ios static files');
execSync('NODE_ENV=production webpack --progress --profile --config scripts/webpack.build-ios.config.js --mode production', { stdio: [0, 1, 2] });
