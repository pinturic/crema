const path = require('path');
const webpack = require('webpack');
const ProgressBarPlugin = require('progress-bar-webpack-plugin');
const chalk = require('chalk');
const Dotenv = require('dotenv-webpack');
const ESLintPlugin = require('eslint-webpack-plugin');

const srcFolder = path.join(__dirname, '../src');
console.log(`Src folder is: ${srcFolder}`);
const resolvedPath = path.resolve('./src');
console.log(`ResolvedPath is: ${resolvedPath}`);
const nodeModulesResolvedPath = path.resolve(__dirname, '../node_modules');
console.log(`NodeModulesResolvedPath is: ${nodeModulesResolvedPath}`);

module.exports = {
  mode: 'development',
  // devtool: 'cheap-module-eval-source-map', // it seams it is broken now
  devtool: 'eval-source-map',
  entry: [
    'webpack-hot-middleware/client',
    './src/app'
  ],
  devServer: {
    contentBase: './dist',
    hot: true
  },
  optimization: {
    moduleIds: 'named'
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/static/'
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new ESLintPlugin({
      extensions: ['js', 'jsx'],
      exclude: [
        `/node_modules/`,
      ],
    }),
    new ProgressBarPlugin({
      format: `  build [:bar] ${chalk.green.bold(':percent')} (:elapsed seconds)`,
      clear: false,
      width: 60
    }),
    new Dotenv({
      path: './config/development.env', // if not simply .env
      safe: false // lets load the .env.example file as well
    })
  ],
  resolve: {
    // Resolve explicit extensions ('') and implicitly '.js' and '.jsx' ones.
    extensions: ['.js', '.jsx'],
    modules: [
      'node_modules',
      resolvedPath,
      nodeModulesResolvedPath
    ],
  },
  module: {
    rules: [{
      test: /\.jsx?$/,
      use: [{
        loader: 'babel-loader',
        options: {
          cacheDirectory: true,
          presets: ['@babel/preset-env',
            {
              plugins: [
                '@babel/plugin-proposal-class-properties'
              ]
            }]
        }
      }],
      include: [srcFolder, resolvedPath]
    },
    {
      test: /\.html$/,
      use: [{
        loader: 'file-loader?name=[name].[ext]'
      }]
    },
    {
      test: /\.css$/,
      use: ['style-loader', 'css-loader']
    },
    {
      test: /\.(png|gif)$/,
      use: [{
        loader: 'url-loader?limit=100000'
      }]
    }
    ]
  },
  externals: {

  }
};
