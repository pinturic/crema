const cordovaLib = require('cordova-lib');
const path = require('path');
const pkg = require('../package.json');

const cordova = cordovaLib.cordova;
const cordovaCreate = require('cordova-create');
const execSync = require('child_process').execSync;

function cdFolder(folder) {
  const oldPath = getCurrentPath();
  process.chdir(folder);
  return oldPath;
}

function getCurrentPath() {
  return process.cwd();
}

function getPlatformPath(platform) {
  const PREFIX = execSync('npm prefix').toString().trim();
  return path.join(PREFIX, 'devices', platform,);
}

function create(platform) {
  return cordovaCreate(
    getPlatformPath(platform),
    pkg.cordova.id,
    pkg.cordova.name,
    {
      lib: {
        www: {
          url: path.join(process.cwd(), 'devices', 'build', platform, 'www'),
          link: true
        }
      }
    }
  );
}

function addPlugins(platform) {
  const oldPath = cdFolder(getPlatformPath(platform));
  return cordova.plugins('add', pkg.cordova.plugins, { fetch: true, save: true })
    .then(() => cdFolder(oldPath))
    .catch(err => console.log('Error in adding plugins', err));
}

function addPlugin(platform, pluginName) {
  const oldPath = cdFolder(getPlatformPath(platform));
  return cordova.plugins('add', pluginName, { fetch: true, save: true })
    .then(() => cdFolder(oldPath))
    .catch(err => console.log('Error in adding plugin', err));
}

function rmPlugin(platform, pluginName) {
  const oldPath = cdFolder(getPlatformPath(platform));
  return cordova.plugins('rm', pluginName).then(() => cdFolder(oldPath));
}

function listPlugins(platform) {
  const oldPath = cdFolder(getPlatformPath(platform));
  return cordova.plugins('list').then((plugins) => {
    cdFolder(oldPath);
    return plugins;
  });
}

function addPlatform(platform) {
  const oldPath = cdFolder(getPlatformPath(platform));
  function transformPlatform(platform) {
    return path.join(oldPath, 'node_modules', `cordova-${platform}`);
  }
  return cordova.platforms('add', transformPlatform(platform), { fetch: true, save: true })
    .then(() => cdFolder(oldPath))
    .catch(err => console.log('Error in adding platform', err));
}

function build(platform, buildMode, options) {
  const oldPath = cdFolder(getPlatformPath(platform));
  return cordova.build({
    platforms: [platform],
    options: [`--${buildMode}`].concat(options)
  }).then(() => cdFolder(oldPath));
}

function emulate(platform, buildMode, options) {
  const oldPath = cdFolder(getPlatformPath(platform));
  return cordova.emulate({
    platforms: [platform],
    options: [`--${buildMode}`].concat(options)
  })
    .then(() => cdFolder(oldPath))
    .catch(err => `Error emulating: ${err}`);
}

function run(platform, buildMode, options) {
  const oldPath = cdFolder(getPlatformPath(platform));
  return cordova.run({
    platforms: [platform],
    options: [`--${buildMode}`, '--device'].concat(options)
  })
    .then(() => cdFolder(oldPath))
    .catch(err => `Error running: ${err}`);
}

function targets(platform, buildMode, options) {
  const oldPath = cdFolder(getPlatformPath(platform));
  return cordova.targets({
    platforms: [platform],
    options: []
  }).then(() => cdFolder(oldPath));
}

module.exports = {
  create,
  addPlatform,
  addPlugins,
  addPlugin,
  rmPlugin,
  listPlugins,
  build,
  emulate,
  run,
  targets
};
