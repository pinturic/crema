
const ncp = require('ncp');
const fs = require('fs');
const path = require('path');

function mkdirSync(path) {
  try {
    fs.mkdirSync(path);
  } catch (e) {
    if (e.code != 'EEXIST') throw e;
  }
}

/**
 * Create a full path
 * @param  {string} dirpath The full path to create
 */
function mkdirpSync(dirpath) {
  const parts = dirpath.split(path.sep);
  for (let i = 1; i <= parts.length; i++) {
    mkdirSync(path.join.apply(null, parts.slice(0, i)));
  }
}

/**
 * Copy the assets from the assets folder to platform one
 * @param  {string}   platform The platform type
 * @param  {string}   from     The assets folder
 * @param  {string}   to       The destination folder
 * @param  {Function} cb       The callback to call when the folder is created
 */
function copyAssets(platform, from, to, cb) {
  const assetsDir = `${from}/assets/`;
  const platformDir = assetsDir + platform;

  console.log(`Copying platform ${platformDir} to ${to}`);
  ncp(platformDir, to, cb);
}

function pathExist(path) {
  try {
    fs.accessSync(path, fs.F_OK);
    return true;
  } catch (e) {
    return false;
  }
}

module.exports = {
  mkdirpSync,
  copyAssets,
  pathExist
};
