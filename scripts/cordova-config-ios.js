#!/usr/bin/env node
/*
	This script prepare the cordova applications
*/


const fs = require('fs');
const execSync = require('child_process').execSync;
const mkdirpSync = require('./utils.js').mkdirpSync;
const pathExist = require('./utils.js').pathExist;
const path = require('path');
const cordovaTasks = require('./cordova-tasks');
const DOMParser = require('xmldom').DOMParser;
const XMLSerializer = require('xmldom').XMLSerializer;
const ncp = require('ncp').ncp;

const PREFIX = execSync('npm prefix').toString().trim();
const PS = path.sep;
const DEVICES_FOLDER = path.join(PREFIX, 'devices');
const IOS_FOLDER = path.join(PREFIX, 'devices', 'ios');
const BUILD_IOS_FOLDER = path.join(PREFIX, 'devices', 'build', 'ios');
const BUILD_IOS_FOLDER_WWW = path.join(PREFIX, 'devices', 'build', 'ios', 'www');
const CORDOVA_IOS_CONFIG = path.join(PREFIX, 'devices', 'ios', 'config.xml');

const baseXml = `<root>
    <platform name="ios">
        <icon src="res/icons/icon-60@3x.png" width="180" height="180" />
        <icon src="res/icons/icon-60.png" width="60" height="60" />
        <icon src="res/icons/icon-60@2x.png" width="120" height="120" />
        <icon src="res/icons/icon-76.png" width="76" height="76" />
        <icon src="res/icons/icon-76@2x.png" width="152" height="152" />
        <icon src="res/icons/icon-40.png" width="40" height="40" />
        <icon src="res/icons/icon-40@2x.png" width="80" height="80" />
        <icon src="res/icons/icon.png" width="57" height="57" />
        <icon src="res/icons/icon@2x.png" width="114" height="114" />
        <icon src="res/icons/icon-72.png" width="72" height="72" />
        <icon src="res/icons/icon-72@2x.png" width="144" height="144" />
        <icon src="res/icons/icon-83.5@2x.png" width="167" height="167" />
        <icon src="res/icons/icon-small.png" width="29" height="29" />
        <icon src="res/icons/icon-small@2x.png" width="58" height="58" />
        <icon src="res/icons/icon-small@3x.png" width="87" height="87" />
        <icon src="res/icons/icon-50.png" width="50" height="50" />
        <icon src="res/icons/icon-50@2x.png" width="100" height="100" />
     </platform><platform name="ios">
        <splash src="res/splashes/Default~iphone.png" width="320" height="480"/>
        <splash src="res/splashes/Default@2x~iphone.png" width="640" height="960"/>
        <splash src="res/splashes/Default-Portrait~ipad.png" width="768" height="1024"/>
        <splash src="res/splashes/Default-Portrait@2x~ipad.png" width="1536" height="2048"/>
        <splash src="res/splashes/Default-Landscape~ipad.png" width="1024" height="768"/>
        <splash src="res/splashes/Default-Landscape@2x~ipad.png" width="2048" height="1536"/>
        <splash src="res/splashes/Default-568h@2x~iphone.png" width="640" height="1136"/>
        <splash src="res/splashes/Default-667h.png" width="750" height="1334"/>
        <splash src="res/splashes/Default-736h.png" width="1242" height="2208"/>
        <splash src="res/splashes/Default-Landscape-736h.png" width="2208" height="1242"/>
    </platform>
    <preference name="SplashScreen" value="screen"/>
    <preference name="AutoHideSplashScreen" value="false" />
    <preference name="SplashScreenDelay" value="5000" />
    <preference name="FadeSplashScreenDuration" value="500" />
    <preference name="ShowSplashScreenSpinner" value="false" />

    <preference name="Orientation" value="all" />
</root>`;

const setupProperties = () => {
  const platform = 'ios';
  mkdirpSync(`devices${path.sep}${platform}${path.sep}/res`);
  ncp(path.resolve('res', platform), path.resolve('devices', platform), (err) => {
    if (err) {
      return console.error(err);
    }

    const fileName = path.resolve('devices', platform, 'config.xml');
    let content = fs.readFileSync(fileName, {
      encoding: 'utf8'
    });
    const doc = new DOMParser().parseFromString(content);
    const baseXmlDoc = new DOMParser().parseFromString(baseXml);
    let child = baseXmlDoc.childNodes[0].firstChild;
    do {
      doc.getElementsByTagName('widget')[0].appendChild(child.cloneNode(true));
      child = child.nextSibling;
    } while (child.nextSibling);
    content = new XMLSerializer().serializeToString(doc);
    fs.writeFileSync(fileName, content);
  });
};

console.log(`Device folder is: ${DEVICES_FOLDER}`);
if (!pathExist(DEVICES_FOLDER)) {
  mkdirpSync('devices');
  console.log('Created');
}

console.log(`Ios folder is: ${IOS_FOLDER}`);
if (!pathExist(IOS_FOLDER)) {
  mkdirpSync(`devices${path.sep}ios`);
  console.log('Created');
}
if (!pathExist(BUILD_IOS_FOLDER)) {
  mkdirpSync(`devices${path.sep}build${path.sep}ios`);
}
if (!pathExist(BUILD_IOS_FOLDER_WWW)) {
  mkdirpSync(`devices${path.sep}build${path.sep}ios${path.sep}www`);
}

if (!pathExist(CORDOVA_IOS_CONFIG)) {
  cordovaTasks.create('ios')
    .then(res => console.log('Ios cordova project ready'))
    .then(() => cordovaTasks.addPlatform('ios'))
    .then(res => console.log('Ios cordova platform ready '))
    .then(() => cordovaTasks.addPlugins('ios'))
    .then(res => console.log('Ios cordova plugins ready '))
    .then(() => setupProperties('ios'))
    .catch(err => console.log('Error in cordova setup for Ios', err));
}

