#!/usr/bin/env node
/*
  This script builds the cordova application for android or ios
*/
const execSync = require('child_process').execSync;
const cordovaTasks = require('./cordova-tasks');

const appName = `${process.argv[1].match(/(?:.*[\/\\])?(.*)$/)[1]}`;

const opts = require('node-getopt').create([
  ['l', 'list', 'Lists all available targets (both devices and emulators)'],
  ['n', 'nobuild', 'Don-\'t build'],
  ['d', 'debug', 'Debug build'],
  ['r', 'release', 'Release build'],
  ['x', 'device', 'Build for a device'],
  ['e', 'emulator', 'Build for emulator'],
  ['t', 'target=ARG', 'Deploy to a specific target'],
  ['p', 'platform=ARG', 'One of android or ios'],
  ['h', 'help', 'display this help']
])
  .bindHelp(`Usage: node ${appName} [MODE] [PROD] [TARGET] [PLAT] [-- POPTS]\n` +
  'Run script for cordova application\n' +
  'MODE:   --list|--nobuild\n' +
  'PROD:   --debug|--release\n' +
  'TARGET: --device|--emulator|--target=<DEVICE_NAME>\n' +
  'PLAT:   --platform=[android|ios]\n' +
  'POPTS:  platformopts\n' +
  '\n' +
  '[[OPTIONS]]\n' +
  '\n' +
  'To provide platform specific options, you must include them after `--`.\n' +
  'Examples:\n' +
  ` node ${appName} -p android -d -x\n` +
  ` node ${appName} --platform=ios --release\n` +
  ` node ${appName} --platform=android --list\n`)
  .parseSystem();

// normalize opt
const options = opts.options;
if (!options.platform) {
  options.platform = 'android';
}
if (!options.debug && !opts.options.release) {
  options.release = true;
}
if (!options.device && !opts.options.emulator) {
  options.device = true;
}

console.info(`Running with: ${
  options.platform
}${options.debug && ' debug' || options.release && ' release'
}${options.device && ' device' || options.emulator && ' emulator' || options.target && (` ${options.target}`)
}${options.list && ' list' || ''
}${options.nobuild && ' nobuild' || ''}`);

const func = options.list ? cordovaTasks.targets : (options.emulator ? cordovaTasks.emulate : cordovaTasks.run);

func(
  options.platform,
  options.debug && 'debug' || options.release && 'release',
  [options.device && '--device' || options.emulator && '--emulator' || options.target && `--target=${options.target}`,
    options.list && '--list' || '',
    options.nobuild && '--nobuild' || ''].concat(opts.argv)
)
  .then(res => console.log(`Run on device: ${res}`))
  .catch(err => console.log(`Error running on device: ${err}`));

