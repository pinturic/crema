#!/usr/bin/env node
/*
	This script builds the cordova application for android or ios
*/
const execSync = require('child_process').execSync;
const cordovaTasks = require('./cordova-tasks');

const appName = `${process.argv[1].match(/(?:.*[\/\\])?(.*)$/)[1]}`;

const opts = require('node-getopt').create([
  ['d', 'debug', 'Debug build'],
  ['r', 'release', 'Release build'],
  ['x', 'device', 'Build for a device'],
  ['e', 'emulator', 'Build for emulator'],
  ['p', 'platform=ARG', 'One of android or ios'],
  ['h', 'help', 'display this help']
])
  .bindHelp(`Usage: node ${appName} [PROD] [TARGET] [PLAT] [-- POPTS]\n` +
  'Build script for cordova application\n' +
  'PROD:   --debug|--release\n' +
  'TARGET: --device|--emulator\n' +
  'PLAT:  --platform=[android|ios]\n' +
  'POPTS:  platformopts\n' +
  '\n' +
  '[[OPTIONS]]\n' +
  '\n' +
  'To provide platform specific options, you must include them after `--`.\n' +
  'Examples:\n' +
  `	node ${appName} -p android -d -x\n` +
  `	node ${appName} --platform=ios --release\n` +
  `	node ${appName} --platform=android --release -- --keystore=\"../android.keystore\" --storePassword=android --alias=mykey\n`)
  .parseSystem();

// normalize opt
const options = opts.options;
if (!options.platform) {
  options.platform = 'android';
}
if (!options.debug && !options.release) {
  options.release = true;
}
if (!options.device && !options.emulator) {
  options.device = true;
}

console.info(`Building for: ${
  options.platform
}${options.debug && ' debug ' || options.release && ' release '
}${options.device && 'device ' || options.emulator && 'emulator'}`);

if (options.platform === 'android') {
  execSync('npm run buildAndroidStaticFiles', { stdio: [0, 1, 2] });
} else if (options.platform === 'ios') {
  execSync('npm run buildIosStaticFiles', { stdio: [0, 1, 2] });
}

cordovaTasks.build(
  options.platform,
  options.debug && 'debug' || options.release && 'release',
  [options.device && '--device' || options.emulator && '--emulator'].concat(opts.argv)
);

