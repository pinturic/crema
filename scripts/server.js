const path = require('path');
const fs = require('fs');
const express = require('express');
const webpack = require('webpack');
const config = require('./webpack.config');

const app = express();
const compiler = webpack(config);

const devPath = `${__dirname}/../assets/dev`;
console.log(`Devpath is: ${devPath}`);

app.use(require('webpack-dev-middleware')(compiler, {
  publicPath: config.output.publicPath
}));

app.use(require('webpack-hot-middleware')(compiler));

// Serve config.js from root otherwise get the default one under /conf.
app.get('/config.js', (req, res) => {
  fs.stat(path.join(devPath, req.path), (err) => {
    res.setHeader('Content-Type', 'application/javascript');
    res.sendFile(req.path, {
      root: devPath
    });
  });
});

// Serve static files.
app.use(express.static(devPath));

app.get('*', (req, res) => {
  res.sendFile(path.join(devPath, 'index.html'));
});

app.listen(8080, 'localhost', (err) => {
  if (err) {
    console.log(err);
    return;
  }

  console.log('==> 🍨  Listening at http://localhost:8080');
});
