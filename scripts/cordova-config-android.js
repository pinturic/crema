#!/usr/bin/env node
/*
	This script prepare the cordova applications
*/


const fs = require('fs');
const execSync = require('child_process').execSync;
const mkdirpSync = require('./utils.js').mkdirpSync;
const pathExist = require('./utils.js').pathExist;
const path = require('path');
const cordovaTasks = require('./cordova-tasks');
const DOMParser = require('xmldom').DOMParser;
const XMLSerializer = require('xmldom').XMLSerializer;
const ncp = require('ncp').ncp;

const PREFIX = execSync('npm prefix').toString().trim();
const PS = path.sep;
const DEVICES_FOLDER = path.join(PREFIX, 'devices');
const ANDROID_FOLDER = path.join(PREFIX, 'devices', 'android');
const BUILD_ANDROID_FOLDER = path.join(PREFIX, 'devices', 'build', 'android');
const BUILD_ANDROID_FOLDER_WWW = path.join(PREFIX, 'devices', 'build', 'android', 'www');
const CORDOVA_ANDROID_CONFIG = path.join(PREFIX, 'devices', 'android', 'config.xml');

const baseXml = `<root>
    <icon src="res/icon.png" />
    <platform name="android">
        <!-- you can use any density that exists in the Android project -->
        <splash src="res/splashes/drawable-land-hdpi/screen.png" density="land-hdpi"/>
        <splash src="res/splashes/drawable-land-ldpi/screen.png" density="land-ldpi"/>
        <splash src="res/splashes/drawable-land-mdpi/screen.png" density="land-mdpi"/>
        <splash src="res/splashes/drawable-land-xhdpi/screen.png" density="land-xhdpi"/>
        <splash src="res/splashes/drawable-port-hdpi/screen.png" density="port-hdpi"/>
        <splash src="res/splashes/drawable-port-ldpi/screen.png" density="port-ldpi"/>
        <splash src="res/splashes/drawable-port-mdpi/screen.png" density="port-mdpi"/>
        <splash src="res/splashes/drawable-port-xhdpi/screen.png" density="port-xhdpi"/>
    </platform>
    <preference name="SplashScreen" value="screen"/>
    <preference name="AutoHideSplashScreen" value="false" />
    <preference name="SplashScreenDelay" value="5000" />
    <preference name="ShowSplashScreenSpinner" value="false" />
    <preference name="FadeSplashScreenDuration" value="500" />
</root>`;

const setupProperties = () => {
  const platform = 'android';
  mkdirpSync(`devices${path.sep}${platform}${path.sep}/res`);
  ncp(path.resolve('res', platform), path.resolve('devices', platform), (err) => {
    if (err) {
      return console.error(err);
    }

    const fileName = path.resolve('devices', platform, 'config.xml');
    let content = fs.readFileSync(fileName, {
      encoding: 'utf8'
    });
    const doc = new DOMParser().parseFromString(content);
    const baseXmlDoc = new DOMParser().parseFromString(baseXml);
    let child = baseXmlDoc.childNodes[0].firstChild;
    do {
      doc.getElementsByTagName('widget')[0].appendChild(child.cloneNode(true));
      child = child.nextSibling;
    } while (child.nextSibling);
    content = new XMLSerializer().serializeToString(doc);
    fs.writeFileSync(fileName, content);
  });
};

console.log(`Device folder is: ${DEVICES_FOLDER}`);
if (!pathExist(DEVICES_FOLDER)) {
  mkdirpSync('devices');
  console.log('Created');
}

console.log(`Android folder is: ${ANDROID_FOLDER}`);
if (!pathExist(ANDROID_FOLDER)) {
  mkdirpSync(`devices${path.sep}android`);
  console.log('Created');
}
if (!pathExist(BUILD_ANDROID_FOLDER)) {
  mkdirpSync(`devices${path.sep}build${path.sep}android`);
}
if (!pathExist(BUILD_ANDROID_FOLDER_WWW)) {
  mkdirpSync(`devices${path.sep}build${path.sep}android${path.sep}www`);
}

if (!pathExist(CORDOVA_ANDROID_CONFIG)) {
  cordovaTasks.create('android')
    .then(res => console.log('Android cordova project ready'))
    .then(() => cordovaTasks.addPlatform('android'))
    .then(res => console.log('Android cordova platform ready '))
    .then(() => cordovaTasks.addPlugins('android'))
    .then(res => console.log('Android cordova plugins ready '))
    .then(() => setupProperties())
    .catch(err => console.log('Error in cordova setup for Android', err));
}

