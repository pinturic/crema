import messages from './messages';
// Messages
import en from './messages.json';
import es from './messages.es.json';

/**
 * A map of the messages available in the package
 * @type {Object}
 */
const MESSAGES = {
  en,
  es
};

export default messages(MESSAGES);
