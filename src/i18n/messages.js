/**
   Remove falsy values (false, null, undefined, 0, NaN, '').
 * @param  {Object} messages The dictionary object to cleanup
 * @return {Array}          The cleaned language mapping
 */
const cleanUp = (messages) => Object.keys(messages).reduce((acc, key) => {
  if (messages[key]) {
    acc[key] = messages[key]; // eslint-disable-line no-param-reassign
  }
  return acc;
}, {});

/**
 * Extend English language with the requested one.
 * First check if the language exists and add the missing items with the ones
 * present in english
 * @param  {Object} MESSAGES The dictionary object to cleanup
 * @return {Object}          The cleaned dictionary
 */
const messages = (MESSAGES) => (language) => ({
  ...MESSAGES.en,
  ...(language in MESSAGES
    && cleanUp(MESSAGES[language]))
});

export default messages;
