import { createMuiTheme, fade } from '@material-ui/core/styles';

/* eslint-disable no-unused-vars, no-trailing-spaces */
/**
 * bright black
 * @type {String}
 */
const base03 = '#002b36';

/**
 * black
 * @type {String}
 */
const base02 = '#073642';

/**
 * bright green
 * @type {String}
 */
const base01 = '#586e75';

/**
 * bright yellow
 * @type {String}
 */
const base00 = '#657b83';

/**
 * bright cyan
 * @type {String}
 */
const base1 = '#93a1a1';  

/**
 * white
 * @type {String}
 */
const base2 = '#eee8d5';

/**
 * bright white
 * @type {String}
 */
const base3 = '#fdf6e3';   

/**
 * yellow
 * @type {String}
 */
const yellow = '#b58900';   

/**
 * bright red
 * @type {String}
 */
const orange = '#cb4b16';      

/**
 * red
 * @type {String}
 */
const red = '#dc322f';       

/**
 * magenta
 * @type {String}
 */
const magenta = '#d33682';  

/**
 * brigth magenta
 * @type {String}
 */
const violet = '#6c71c4';   

/**
 * blue
 * @type {String}
 */
const blue = '#268bd2';     

/**
 * cyan
 * @type {String}
 */
const cyan = '#2aa198';     

/**
 * green
 * @type {String}
 */
const green = '#859900';       
/* eslint-enable no-unused-vars */

/**
 * A solarize theme for crema {@link http://ethanschoonover.com/solarized}.
 * @type {Object}
 */
const solarizedTheme = {
  fontFamily: 'Roboto, sans-serif',
  palette: {
    primary: {
      main: yellow
    },
    secondary: {
      main: orange
    },

    primary1Color: yellow,
    primary2Color: fade(yellow, 0.2),
    primary3Color: base1,
    accent1Color: orange,
    accent2Color: fade(orange, 0.2),
    accent3Color: base1,
    textColor: base00,
    alternateTextColor: base3,
    canvasColor: base3,
    borderColor: base1,
    disabledColor: base2,
    pickerHeaderColor: base00,
    clockCircleColor: violet,
    shadowColor: base1,
  },
};

export default createMuiTheme(solarizedTheme);
