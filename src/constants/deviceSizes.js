/**
 * The breakpoints for the devices: less than 480px wide is a mobile phone
 * greater than 980 px is a desktop, tablet otherwise
 * @type {Object}
 */
const DEVICE_SIZES = {
  DESKTOP: 980,
  PHONE: 480
};

export default DEVICE_SIZES;
