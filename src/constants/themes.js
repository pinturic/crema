import { createMuiTheme } from '@material-ui/core/styles';
import blue from '@material-ui/core/colors/blue';
import pink from '@material-ui/core/colors/pink';

import solarizedTheme from './solarizedTheme';

const darkBaseTheme = createMuiTheme({
  palette: {
    type: 'dark',
    primary: blue,
    secondary: pink,
  },
});
const lightBaseTheme = createMuiTheme({
  palette: {
    type: 'light',
    primary: blue,
    secondary: pink,
  },
});

const themes = {
  dark: darkBaseTheme,
  light: lightBaseTheme,
  solarized: solarizedTheme,
};

export default themes;
