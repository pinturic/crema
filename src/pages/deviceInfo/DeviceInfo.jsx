import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import AppQuery from 'components/device/AppQuery';
import PhoneSize from 'components/device/PhoneSize';
import TabletSize from 'components/device/TabletSize';
import DesktopSize from 'components/device/DesktopSize';
import DEVICE_SIZES from 'constants/deviceSizes';
import BackButtonHandler from 'components/BackButtonHandler';
import messages from 'i18n';
import getLogger from 'utils/logger';
import { IntlProvider, FormattedMessage } from 'react-intl';
import * as deviceInfoActions from './actions/deviceInfo';

/**
 * The styles for this page
 * @type {Object}
 */
const styles = {
  container: {
    margin: '10px'
  },
  key: {
    display: 'block'
  }
};

/**
 * A page the shows the general infos for the device
 * @return {Object}      The page
 */
class DeviceInfo extends React.Component { // eslint-disable-line

  /*
  constructor() {
    super();
  }
  */

  /**
   * The rendering for the DeviceInfo page
   * @return {Object} The rendered Page
   */
  render() {
    // eslint-disable-next-line no-undef
    const myDevice = typeof device === 'undefined' ? {} : device;
    const { language } = this.props;
    return (
      <IntlProvider
        locale={language}
        messages={messages(language)}
      >
        <div style={styles.container}>
          <Card>
            <CardHeader
              title={<FormattedMessage id="deviceInfo.title" />}
              subtitle={<FormattedMessage id="deviceInfo.subTitle" />}
            />
            <CardContent>
              <AppQuery isDevice>
                <FormattedMessage id="deviceInfo.cordova" />
                {
                  Object.keys(myDevice).map((key) => (
                    <div style={styles.key}>
                      {key}
                      {' '}
                      -&gt;
                      {`${myDevice[key]}`}
                    </div>
                  ))
                }
              </AppQuery>
              <AppQuery isDevice={false}>
                <FormattedMessage id="deviceInfo.browser" />
              </AppQuery>
              <PhoneSize>
                <FormattedMessage
                  id="deviceInfo.phone"
                  values={{
                    size: DEVICE_SIZES.PHONE
                  }}
                />
              </PhoneSize>
              <TabletSize>
                <FormattedMessage
                  id="deviceInfo.tablet"
                  values={{
                    sizePhone: DEVICE_SIZES.PHONE,
                    sizeDesktop: DEVICE_SIZES.DESKTOP,
                  }}
                />
              </TabletSize>
              <DesktopSize>
                <FormattedMessage
                  id="deviceInfo.desktop"
                  values={{
                    size: DEVICE_SIZES.DESKTOP
                  }}
                />
              </DesktopSize>
            </CardContent>
          </Card>
          <BackButtonHandler
            onBack={() => {
              navigator.Backbutton.goBack(() => {
                getLogger().info('Home go success');
              }, () => {
                getLogger().info('Home go fail');
              });
            }}
          />
        </div>
      </IntlProvider>
    );
  }
}

DeviceInfo.propTypes = {
  language: PropTypes.string.isRequired
};

/**
 * Maps the store state to the props for this page
 * @param  {Object} state The global state of the application
 * @return {Object}       The mapping for the state
 */
function mapStateToProps(state) {
  return {
    deviceInfo: state.deviceInfo
  };
}

/**
 * Maps the actions to the props for this page
 * @param  {Object} dispatch The dispatch for this store
 * @return {Object}          The mapping for the actions
 */
function mapDispatchToProps(dispatch) {
  return {
    deviceInfoActions: bindActionCreators(deviceInfoActions, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DeviceInfo);
