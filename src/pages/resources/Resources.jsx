import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import Paper from '@material-ui/core/Paper';
import smallCrema from 'resources/img/crema_small.png';
import FileCloudUpload from '@material-ui/icons/CloudUpload';
import messages from 'i18n';
import { IntlProvider, FormattedMessage } from 'react-intl';
import crema from 'resources/img/crema.png';
import { connect } from 'react-redux';
import getLogger from 'utils/logger';
import BackButtonHandler from 'components/BackButtonHandler';
import { changePage } from 'app/actions/app';
import * as deviceInfoActions from './actions/resources';

/**
 * The styles defined for this page
 * @type {Object}
 */
const styles = {
  container: {
    margin: '10px',
    width: '100%'
  },
  bigImg: {
    height: 140,
    width: 140,
    margin: 20,
    textAlign: 'center',
    display: 'inline-block',
  },
  smallImg: {
    height: 70,
    width: 70,
    margin: 20,
    textAlign: 'center',
    display: 'inline-block',
  },
  text: {
    height: 35,
    width: 35,
    margin: 20,
    textAlign: 'center',
    display: 'inline-block',
  },
};

/**
 * A page that shows the resources loaded from the package
 * @return {Object}      The page
 */
class Resources extends React.Component {
  /*
  constructor() {
    super();
  }
  */

  /**
   * Called when the component is mounted
   * @return {Object} Does not return anything
   */
  componentDidMount = () => {
    const logger = getLogger();
    logger.debug('Resource page has mounted');
  }

  /**
   * Called when the component is unmounted
   * @return {Object} Does not return anything
   */
  componentWillUnmount = () => {
    const logger = getLogger();
    logger.debug('Resource page will unmount');
  }

  /**
   * The rendering for the DeviceInfo page
   * @return {Object} The rendered Page
   */
  render() {
    const { RESOURCE_STRING } = process.env;
    const { changePage: changePageAction, language } = this.props;
    return (
      <IntlProvider
        locale={language}
        messages={messages(language)}
      >
        <div>
          <div style={styles.container}>
            <Paper style={styles.bigImg} elevation={3}>
              <img style={styles.image} src={crema} alt="Big CReMa" />
            </Paper>
            <FormattedMessage id="resources.bigImg" />
          </div>
          <div style={styles.container}>
            <Paper style={styles.smallImg} elevation={3}>
              <img style={styles.image} src={smallCrema} alt="Small CReMa" />
            </Paper>
            <FormattedMessage id="resources.smallImg" />
          </div>
          <div style={styles.container}>
            <Paper style={styles.text} elevation={3}>
              <FileCloudUpload style={styles.image} />
            </Paper>
            {RESOURCE_STRING}
          </div>
          <BackButtonHandler
            onBack={() => changePageAction('deviceInfo')}
          />
        </div>
      </IntlProvider>
    );
  }
}

Resources.propTypes = {
  language: PropTypes.string,
  changePage: PropTypes.func.isRequired
};

Resources.defaultProps = {
  language: 'en',
};

/**
 * Maps the store state to the props for this page
 * @param  {Object} state The global state of the application
 * @return {Object}       The mapping for the state
 */
function mapStateToProps(state) {
  return {
    resources: state.resources
  };
}

/**
 * Maps the actions to the props for this page
 * @param  {Object} dispatch The dispatch for this store
 * @return {Object}          The mapping for the actions
 */
function mapDispatchToProps(dispatch) {
  return {
    deviceInfoActions: bindActionCreators(deviceInfoActions, dispatch),
    changePage: bindActionCreators(changePage, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Resources);
