import { FETCH_API_RECEIVED, FETCH_LOADING } from '../constants/ActionTypes';

/**
 * The initial state for the reducer
 * @type {Object}
 */
const initialState = {
  searchValue: 'CReMa',
  data: null
};

/**
 * The reducer for the FetchApi page
 * @param  {Object} state The actual state of the reducer
 * @param  {Object} action The action that modifies the state
 * @return {Object}        The new state
 */
export default function fetchReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_LOADING:
      return { ...state, data: null };

    case FETCH_API_RECEIVED:
      return { ...state, data: action.data };

    default:
      return state;
  }
}
