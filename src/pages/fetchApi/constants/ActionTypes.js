/**
 * Triggers the action notifying that the data has been received
 * @type {String}
 */
export const FETCH_API_RECEIVED = 'FETCH_API_RECEIVED';

/**
 * Triggers the action notifying that the data is being loaded
 * @type {String}
 */
export const FETCH_LOADING = 'FETCH_LOADING';
