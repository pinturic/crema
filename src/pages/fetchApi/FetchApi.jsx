import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import messages from 'i18n';
import { IntlProvider, FormattedMessage } from 'react-intl';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import Avatar from '@material-ui/core/Avatar';
import ChatBubble from '@material-ui/icons/ChatBubble';
import Loading from 'components/Loading';
import { connect } from 'react-redux';
import BackButtonHandler from 'components/BackButtonHandler';
import { changePage as changePageAction } from 'app/actions/app';
import * as fetchApiActionsFile from './actions/fetchApi';

/**
 * The stayles for this page
 * @type {Object}
 */
const styles = {
  container: {
    margin: '10px'
  }
};

/**
 * This class shows how to use the fetch api to get data
 * @return {Object}      The page
 */
class FetchApi extends React.Component {
  /**
   * Called when the component mounts
   * @return {Object} Does not return anything
   */
  componentDidMount = () => {
    const {
      fetchApiActions
    } = this.props;
    fetchApiActions.getImages();
  }

  /**
   * The render function
   * @return {Object} Rendered data
   */
  render() {
    const {
      changePage,
      fetchApi,
      language
    } = this.props;
    return (
      <IntlProvider
        locale={language}
        messages={messages(language)}
      >
        <div style={styles.container}>
          <Loading loading={!fetchApi.data}>
            <List>
              <ListSubheader>
                <FormattedMessage id="fetchApi.textSearch" />
              </ListSubheader>
              {
                fetchApi.data && fetchApi.data.images.map((image) => (
                  <ListItem key={image.url}>
                    <ListItemAvatar>
                      <Avatar src={image.url} />
                    </ListItemAvatar>
                    <ListItemText
                      key={`img-${image.text}`}
                      primary={image.text}
                    />
                    <ListItemSecondaryAction>
                      <ChatBubble />
                    </ListItemSecondaryAction>
                  </ListItem>
                ))
              }
            </List>
          </Loading>
          <BackButtonHandler
            onBack={() => changePage('deviceInfo')}
          />
        </div>
      </IntlProvider>
    );
  }
}

FetchApi.propTypes = {
  language: PropTypes.string,
  fetchApiActions: PropTypes.object.isRequired,
  fetchApi: PropTypes.object.isRequired,
  changePage: PropTypes.func.isRequired
};

FetchApi.defaultProps = {
  language: 'en',
};

/**
 * Maps the store state to the props for this page
 * @param  {Object} state The global state of the application
 * @return {Object}       The mapping for the state
 */
function mapStateToProps(state) {
  return {
    fetchApi: state.fetchApi
  };
}

/**
 * Maps the actions to the props for this page
 * @param  {Object} dispatch The dispatch for this store
 * @return {Object}          The mapping for the actions
 */
function mapDispatchToProps(dispatch) {
  return {
    fetchApiActions: bindActionCreators(fetchApiActionsFile, dispatch),
    changePage: bindActionCreators(changePageAction, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FetchApi);
