import fetch from 'utils/fetch';
import { FETCH_API_RECEIVED, FETCH_LOADING } from '../constants/ActionTypes';

/**
 * Called when receiving the remote API data
 * @param  {Object} data The returned data from the server
 * @return {Object}      The generated action
 */
export function fetchApiReceived(data) {
  return { type: FETCH_API_RECEIVED, data };
}

/**
 * Set the loading status for the page
 * @return {Object} The generated action
 */
export function fetchLoading() {
  return { type: FETCH_LOADING };
}

/**
 * Download the images from the server
 * @return {Object} The generated action
 */
export function getImages() {
  return (dispatch) => {
    dispatch(fetchLoading);
    return fetch('http://private-3a68c-crema.apiary-mock.com/images', {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    })
      .then((response) => response.json())
      .then((response) => dispatch(fetchApiReceived(response)));
  };
}
