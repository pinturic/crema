import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import messages from 'i18n';
import BackButtonHandler from 'components/BackButtonHandler';
import { changePage } from 'app/actions/app';
import {
  IntlProvider, FormattedMessage, defineMessages, injectIntl
}
  from 'react-intl';
import AddIcon from '@material-ui/icons/Add';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Fab from '@material-ui/core/Fab';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import FormLabel from '@material-ui/core/FormLabel';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import Select from '@material-ui/core/Select';
import Slider from '@material-ui/core/Slider';
import Switch from '@material-ui/core/Switch';
import TextField from '@material-ui/core/TextField';
import * as guiSampleActions from './actions/guiSample';

/**
 * The styles defined for this page
 * @type {Object}
 */
const styles = {
  outsideContainer: {
    margin: '10px'
  },
  containerCentered: {
    margin: '10px'
  }
};

/**
 * The string messages used in this page
 * @type {Object}
 */
const stringMessages = defineMessages({
  dialogStandard: {
    id: 'guiSample.dialogStandard',
  }
});

/**
 * A page that shows samples for the UI
 * @return {Object}      The page
 */
class GuiSample extends React.Component {
  /**
   * The constructor for this page. Sets the state for the object.
   * @return {Object} The GuiSample page
   */
  constructor() {
    super();
    /**
     * This is keeping the state of the dialogs and the snack bars
     * @type {Object}
     */
    this.state = {
      dialogOpen: false
    };
  }

  /**
   * Called when the dialog is to be open
   * @return {Object} Does not return anything
   */
  handleTouchTapDialog = () => {
    this.setState({
      dialogOpen: true,
    });
  }

  /**
   * Called when the dialog is to be closed
   * @return {Object} Does not return anything
   */
  handleRequestCloseDialog = () => {
    this.setState({
      dialogOpen: false,
    });
  }

  /**
   * The rendering for the DeviceInfo page
   * @return {Object} The rendered Page
   */
  render() {
    /**
     * The utility function to handle translations
     */
    const {
      changePage: changePageAction,
      intl,
      language,
    } = this.props;
    const { dialogOpen } = this.state;
    const { formatMessage } = intl;

    return (
      <IntlProvider
        locale={language}
        messages={messages(language)}
      >
        <div style={styles.outsideContainer}>
          <div style={styles.group}>
            <div style={styles.containerCentered}>
              <Fab color="primary" aria-label="Add">
                <AddIcon />
              </Fab>
            </div>
            <div style={styles.containerCentered}>
              <Button variant="contained" color="secondary">
                <FormattedMessage id="guiSample.secondary" />
              </Button>
            </div>
            <div style={styles.containerCentered}>
              <Button variant="contained" color="primary">
                <FormattedMessage id="guiSample.primary" />
              </Button>
            </div>
            <div style={styles.containerCentered}>
              <Button variant="contained">
                <FormattedMessage id="guiSample.default" />
              </Button>
            </div>
          </div>
          <div style={styles.group}>
            <div style={styles.container}>
              <FormControlLabel
                control={(
                  <Checkbox
                    value="checkboxName1"
                    color="primary"
                  />
                )}
                label="checkbox"
              />
              <FormControlLabel
                control={(
                  <Checkbox
                    value="checkboxName2"
                    color="primary"
                    disabled
                  />
                )}
                label="checkbox disabled"
              />
            </div>
            <div style={styles.container}>
              <FormControl component="fieldset">
                <FormLabel component="legend">Gender</FormLabel>
                <RadioGroup
                  aria-label="Gender"
                  name="gender1"
                  value="male"
                >
                  <FormControlLabel
                    value="female"
                    control={<Radio />}
                    label="Female"
                  />
                  <FormControlLabel
                    value="male"
                    control={<Radio />}
                    label="Male"
                  />
                  <FormControlLabel
                    value="other"
                    control={<Radio />}
                    label="Other"
                  />
                  <FormControlLabel
                    value="disabled"
                    disabled
                    control={<Radio />}
                    label="(Disabled option)"
                  />
                </RadioGroup>
              </FormControl>
            </div>
            <div style={styles.container}>
              <FormGroup row>
                <FormControlLabel
                  control={
                    <Switch checked value="checkedA" />
                  }
                  label="Secondary"
                />
                <FormControlLabel
                  control={(
                    <Switch
                      checked
                      value="checkedB"
                      color="primary"
                    />
                  )}
                  label="Primary"
                />
                <FormControlLabel
                  control={<Switch value="checkedC" />}
                  label="Uncontrolled"
                />
                <FormControlLabel
                  disabled
                  control={<Switch value="checkedD" />}
                  label="Disabled"
                />
                <FormControlLabel
                  disabled
                  control={<Switch checked value="checkedE" />}
                  label="Disabled"
                />
              </FormGroup>
            </div>
          </div>
          <div style={{ ...styles.group, marginTop: 0 }}>
            <div style={styles.container}>
              <TextField
                placeholder="TextField"
              />
            </div>
            <div style={styles.container}>
              <TextField
                id="date"
                label="Birthday"
                type="date"
                placeholder={intl.formatMessage({
                  id: 'guiSample.landscapeDialog'
                })}
                defaultValue="2017-05-24"
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </div>
            <div style={styles.container}>
              <FormControl>
                <InputLabel htmlFor="age-simple">Age</InputLabel>
                <Select
                  value={3}
                  inputProps={{
                    name: 'age',
                    id: 'age-simple',
                  }}
                >
                  <MenuItem
                    value={1}
                  >
                    <FormattedMessage id="guiSample.never" />
                  </MenuItem>
                  <MenuItem
                    value={2}
                  >
                    <FormattedMessage id="guiSample.everyNight" />
                  </MenuItem>
                  <MenuItem
                    value={3}
                  >
                    <FormattedMessage id="guiSample.weekNights" />
                  </MenuItem>
                  <MenuItem
                    value={4}
                  >
                    <FormattedMessage id="guiSample.weekEnds" />
                  </MenuItem>
                  <MenuItem
                    value={5}
                  >
                    <FormattedMessage id="guiSample.weekly" />
                  </MenuItem>
                </Select>
              </FormControl>
            </div>
          </div>
          <div style={styles.groupSlider}>
            <Slider
              value={0.5}
              aria-labelledby="label"
            />
          </div>
          <div style={styles.group}>
            <div style={styles.containerCentered}>
              <Button onClick={this.handleTouchTapDialog}>
                <FormattedMessage id="guiSample.viewDialog" />
              </Button>
              <Dialog
                open={dialogOpen}
                onClose={this.handleRequestCloseDialog}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
              >
                <DialogTitle id="alert-dialog-title">
                  { formatMessage(stringMessages.dialogStandard)}
                </DialogTitle>
                <DialogContent>
                  <DialogContentText id="alert-dialog-description">
                    <FormattedMessage id="guiSample.testPageComponents" />
                  </DialogContentText>
                </DialogContent>
                <DialogActions>
                  <Button
                    color="primary"
                    onClick={this.handleRequestCloseDialog}
                  >
                    <FormattedMessage id="guiSample.cancel" />
                  </Button>
                  <Button
                    color="secondary"
                    onClick={this.handleRequestCloseDialog}
                  >
                    <FormattedMessage id="guiSample.submit" />
                  </Button>
                </DialogActions>
              </Dialog>
            </div>
          </div>
          <BackButtonHandler
            onBack={() => changePageAction('deviceInfo')}
          />
        </div>
      </IntlProvider>
    );
  }
}

GuiSample.propTypes = {
  language: PropTypes.string.isRequired,
  intl: PropTypes.object.isRequired,
  changePage: PropTypes.func.isRequired
};

/**
 * Maps the store state to the props for this page
 * @param  {Object} state The global state of the application
 * @return {Object}       The mapping for the state
 */
function mapStateToProps(state) {
  return {
    guiSample: state.guiSample
  };
}

/**
 * Maps the actions to the props for this page
 * @param  {Object} dispatch The dispatch for this store
 * @return {Object}          The mapping for the actions
 */
function mapDispatchToProps(dispatch) {
  return {
    guiSampleActions: bindActionCreators(guiSampleActions, dispatch),
    changePage: bindActionCreators(changePage, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectIntl(GuiSample));
