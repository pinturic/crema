import { push } from 'connected-react-router';
import { NAV_STATUS, NAV_CHANGE_LANGUAGE, NAV_CHANGE_THEME }
  from '../constants/ActionTypes';

/**
 * Set the status for the menu
 * @param {Boolean} status True if the menu is open
 * @return {Object}          The generated action
 */
export function setMenu(status) {
  return { type: NAV_STATUS, status };
}

/**
 * Change the main language for the application
 * @param  {String} language The new Language
 * @return {Object}          The generated action
 */
export function changeLanguage(language) {
  return { type: NAV_CHANGE_LANGUAGE, language };
}

/**
 * Change the theme for the application
 * @param  {Object} theme The new theme
 * @return {Object}       The generated action
 */
export function changeTheme(theme) {
  return { type: NAV_CHANGE_THEME, theme };
}

/**
 * Change the status of the menu (open/closed)
 * @return {Object} The generated action
 */
export function toggleMenu() {
  return (dispatch, status) => dispatch(setMenu(!status().app.menu));
}

/**
 * Changes the pages active on screen
 * @param  {Object} newPage The new page to show
 * @param  {Object} newParams The new params to pass to the page
 * @return {Object}         The generated action
 */
export function changePage(newPage, newParams) {
  return (dispatch) => {
    // remove undefined or null values
    const validParams = {};
    Object.keys(newParams || {}).forEach((key) => {
      // I don't want to add the _k param it is added by the routing library
      // and it stores its correct value
      if (newParams[key] !== undefined && newParams[key] !== null
          && key !== '_k') {
        validParams[key] = newParams[key];
      }
    });
    dispatch(push({
      pathname: newPage,
      query: validParams
    }));
  };
}
