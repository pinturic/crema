import {
  applyMiddleware,
  createStore,
  combineReducers,
  // compose
} from 'redux';
import { connectRouter, routerMiddleware } from 'connected-react-router';

import thunk from 'redux-thunk';
import rootReducer from '../reducers';

/**
 * Configure the global store of the application
 * @param  {Object} history The history middleware
 * @param  {Object} initialState The optional initialState read from the store
 * @return {Object}         Returns the configured store
 */
export default function configureStore(history, initialState) {
  const browserMiddleware = routerMiddleware(history);

  const createStoreWithMiddleware = applyMiddleware(
    thunk,
    browserMiddleware // Also added this line!
  )(createStore);
  const store = createStoreWithMiddleware(combineReducers({
    ...rootReducer,
    router: connectRouter(history),
  }), initialState);

  return store;
  /*
  const store = createStore(
    combineReducers({
      router: connectRouter(history),
      ...rootReducer,
    }),
    initialState,
    compose(
      applyMiddleware(
        routerMiddleware(history), // for dispatching history actions
        thunk,
      ),
    ),
  );

  return store;
  */
}
