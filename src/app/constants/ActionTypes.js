/**
 * Set the status for the menu: true when the menu is to be opened]
 * @type {String}
 */
export const NAV_STATUS = 'NAV_STATUS';

/**
 * Change the main language for the application
 * @type {String}
 */
export const NAV_CHANGE_LANGUAGE = 'NAV_CHANGE_LANGUAGE';

/**
 * Change the theme for the application
 * @type {String}
 */
export const NAV_CHANGE_THEME = 'NAV_CHANGE_THEME';
