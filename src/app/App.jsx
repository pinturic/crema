/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import NavigationMoreVert from '@material-ui/icons/MoreVert';
import ActionSettingsBackupRestore
  from '@material-ui/icons/SettingsBackupRestore';
import DeviceNetworkWifi from '@material-ui/icons/NetworkWifi';
import CommunicationPhonelinkSetup from '@material-ui/icons/PhonelinkSetup';
import FileFolder from '@material-ui/icons/Folder';
import { Route, Redirect } from 'react-router';
// all the pages available to the app
import DeviceInfo from 'pages/deviceInfo/DeviceInfo';
import GuiSample from 'pages/guiSample/GuiSample';
import Resources from 'pages/resources/Resources';
import FetchApi from 'pages/fetchApi/FetchApi';

import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import MenuItem from '@material-ui/core/MenuItem';
import AppBar from '@material-ui/core/AppBar';
import Drawer from '@material-ui/core/Drawer';
import Avatar from '@material-ui/core/Avatar';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import Menu from '@material-ui/core/Menu';
import Grid from '@material-ui/core/Grid';
import ListItemIcon from '@material-ui/core/ListItemIcon';

import messages from 'i18n';
import { IntlProvider, FormattedMessage } from 'react-intl';
import smallCrema from 'resources/img/crema_small.png';
// these are used to set up Material-ui theme:
// http://www.material-ui.com/#/get-started/installation

import { MuiThemeProvider } from '@material-ui/core/styles';
import * as appActionsFile from './actions/app';
import themes from '../constants/themes';

/**
 * The global styles for the application header and text
 * @type {Object}
 */
const styles = {
  header: {
    padding: 20
  },
  title: {
    flexGrow: 1,
  },
  headerText: {
    marginLeft: 10,
    fontSize: 20,
    verticalAlign: 'top',
    lineHeight: '44px'
  }
};

/**
 * The main Application component
 * @return {Object}      The page
 */
class App extends React.Component {
  /**
   * The constructor
   * @param  {[type]} props [description]
   * @return {[type]}       [description]
   */
  constructor(props) {
    super(props);
    this.state = {
      anchorEl: null,
    };
  }

  /**
   * Called when the component mounts
   * @return {Object} Does not return anything
   */
  componentDidMount() {
    const { app } = this.props;
    document.body.style.backgroundColor = themes[app.theme]
      .palette.background.default;
  }

  /**
   * Called when the component receive new properties
   * @param  {Object} newProps The new properties
   * @return {Object}          Does not return anything
   */
  componentDidUpdate() {
    const { app } = this.props;
    document.body.style.backgroundColor = themes[app.theme]
      .palette.background.default;
  }

  /**
   * Triggered whrn the menu in the left navigation bar receives a click
   * @param  {Object} newPage The new page to show
   * @param {Event} e The onClick event
   * @return {Object}         Does not return anything
   */
  menuClick(newPage, e) {
    e.preventDefault();
    const { appActions } = this.props;
    appActions.changePage(newPage);
    appActions.setMenu(false);
  }

  /**
   * Chnages the language for the application
   * @param  {String} newLanguage The iso code of the language
   * @return {Object}             Does not return anything
   */
  languageSelected(newLanguage) {
    const { appActions } = this.props;
    appActions.changeLanguage(newLanguage);
  }

  /**
   * Changes the theme for the application
   * @param  {Object} newTheme The new theme to use for the application
   * @return {Object}          Does not return anything
   */
  changeTheme(newTheme) {
    const { appActions, location } = this.props;
    appActions.changeTheme(newTheme);
    appActions.changePage(location.pathname);
  }

  /**
   * The render function for the application
   * @return {Object} Rendered data
   */
  render() {
    const { app, appActions } = this.props;
    const { anchorEl } = this.state;
    /**
     * Opens the settings menu
     * @param  {[type]} event [description]
     * @return {[type]}       [description]
     */
    const handleMenu = (event) => this.setState({
      anchorEl: event.currentTarget
    });
    /**
     * Closes the settings menu
     * @return {[type]} [description]
     */
    const handleClose = () => this.setState({
      anchorEl: null
    });
    const open = Boolean(anchorEl);
    return (
      <MuiThemeProvider theme={themes[app.theme]}>
        <IntlProvider
          locale={app.language}
          messages={messages(app.language)}
        >
          <div>
            <Drawer
              variant="temporary"
              open={app.menu}
              onClose={appActions.toggleMenu}
            >
              <div style={
                styles.header
              }
              >
                <Grid container justify="flex-start" alignItems="center">
                  <Avatar style={styles.avatar} src={smallCrema} />
                  <span style={styles.headerText}>
                    CReMa
                  </span>
                </Grid>
              </div>
              <Divider />
              <MenuItem
                onClick={(e) => this.menuClick('deviceInfo', e)}
              >
                <ListItemIcon><CommunicationPhonelinkSetup /></ListItemIcon>
                <FormattedMessage id="deviceInfo.title" />
              </MenuItem>
              <MenuItem
                onClick={(e) => this.menuClick('guiSample', e)}
              >
                <ListItemIcon><FileFolder /></ListItemIcon>
                <FormattedMessage id="guiSample.title" />
              </MenuItem>
              <MenuItem
                onClick={(e) => this.menuClick('resources', e)}
              >
                <ListItemIcon><ActionSettingsBackupRestore /></ListItemIcon>

                <FormattedMessage id="resources.title" />
              </MenuItem>
              <MenuItem
                onClick={(e) => this.menuClick('fetchApi', e)}
              >
                <ListItemIcon><DeviceNetworkWifi /></ListItemIcon>

                <FormattedMessage id="fetchApi.title" />
              </MenuItem>
            </Drawer>
            <AppBar position="relative">
              <Toolbar>
                <IconButton
                  edge="start"
                  color="inherit"
                  onClick={appActions.toggleMenu}
                  aria-label="Menu"
                >
                  <MenuIcon />
                </IconButton>
                <div style={
                  styles.title
                }
                >
                  <Typography variant="h6">
                    CReMa
                  </Typography>
                </div>
                <IconButton
                  edge="start"
                  color="inherit"
                  aria-label="Menu"
                  onClick={handleMenu}
                >
                  <NavigationMoreVert />
                </IconButton>
                <Menu
                  id="menu-appbar"
                  anchorEl={anchorEl}
                  anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                  }}
                  transformOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                  }}
                  open={open}
                  onClose={this.handleClose}
                >
                  <MenuItem
                    onClick={() => {
                      this.languageSelected('en');
                      handleClose();
                    }}
                  >
                    <FormattedMessage id="language.english" />
                  </MenuItem>
                  <MenuItem
                    onClick={() => {
                      this.languageSelected('es');
                      handleClose();
                    }}
                  >
                    <FormattedMessage id="language.spanish" />
                  </MenuItem>
                  <Divider />
                  <MenuItem
                    onClick={() => {
                      this.changeTheme('solarized');
                      handleClose();
                    }}
                  >
                    <FormattedMessage id="theme.solarized" />
                  </MenuItem>
                  <MenuItem
                    onClick={() => {
                      this.changeTheme('light');
                      handleClose();
                    }}
                  >
                    <FormattedMessage id="theme.light" />
                  </MenuItem>
                  <MenuItem
                    onClick={() => {
                      this.changeTheme('dark');
                      handleClose();
                    }}
                  >
                    <FormattedMessage id="theme.dark" />
                  </MenuItem>
                </Menu>

              </Toolbar>
            </AppBar>
            <Route
              exact
              path="/"
              render={() => <Redirect to="/DeviceInfo" component={App} />}
            />
            <Route
              path="/deviceInfo"
              render={(props) => (
                <DeviceInfo
                  {...props}
                  language={app.language}
                />
              )}
            />
            <Route
              path="/guiSample"
              render={(props) => (
                <GuiSample
                  {...props}
                  language={app.language}
                />
              )}
            />
            <Route
              path="/resources"
              render={(props) => (
                <Resources
                  {...props}
                  language={app.language}
                />
              )}
            />
            <Route
              path="/fetchApi"
              render={(props) => (
                <FetchApi
                  {...props}
                  language={app.language}
                />
              )}
            />
          </div>
        </IntlProvider>
      </MuiThemeProvider>
    );
  }
}

App.propTypes = {
  appActions: PropTypes.object.isRequired,
  app: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired
};

/**
 * Maps the store state to the props for this page
 * @param  {Object} state The global state of the application
 * @return {Object}       The mapping for the state
 */
function mapStateToProps(state) {
  return {
    app: state.app
  };
}

/**
 * Maps the actions to the props for this page
 * @param  {Object} dispatch The dispatch for this store
 * @return {Object}          The mapping for the actions
 */
function mapDispatchToProps(dispatch) {
  return {
    appActions: bindActionCreators(appActionsFile, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
