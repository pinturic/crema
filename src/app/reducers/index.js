import fetchReducer from 'pages/fetchApi/reducers/fetchApi';
import appReducer from './app';

/**
 * The root reducer for the store
 * @type {Function}
 */
const rootReducer = {
  app: appReducer,
  fetchApi: fetchReducer
};

export default rootReducer;
