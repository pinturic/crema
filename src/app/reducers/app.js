import { NAV_STATUS, NAV_CHANGE_LANGUAGE, NAV_CHANGE_THEME } from
  '../constants/ActionTypes';

/**
 * The initial state for the app
 * @type {Object}
 */
const initialState = {
  menu: false,
  language: 'en',
  theme: 'solarized',
};

/**
 * The reducer for the app
 * @param  {Object} state The actual state of the reducer
 * @param  {Object} action The action that modifies the state
 * @return {Object}        The new state
 */
export default function appReducer(state = initialState, action) {
  switch (action.type) {
    case NAV_CHANGE_THEME:
      return {
        ...state,
        theme: action.theme
      };

    case NAV_STATUS:
      return {
        ...state,
        menu: action.status
      };

    case NAV_CHANGE_LANGUAGE:
      return {
        ...state,
        language: action.language
      };

    default:
      return state;
  }
}
