import React from 'react';

/**
 * Closes the splashscreen in cordova app
 */
class CloseSplashScreen extends React.Component {
  /**
   * Called whrn this component is mounted
   * @return {Object} The component
   */
  componentDidMount() {
    if (window.navigator && window.navigator.splashscreen) {
      window.navigator.splashscreen.hide();
    }
  }

  /**
   * This compoenent does not show anything
   * @return {bool} Always false
   */
  render() {
    return false;
  }
}

export default CloseSplashScreen;
