/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import deviceIsApp from 'utils/device';
import PropTypes from 'prop-types';

/**
 * It renders its content if the isApp property is tru and we are running
 * the embedded app or isApp is false and we are not on running an app.
 * It is a component and not a steless function because it can render a null
 * component (and stateless function cannot render a null)
 */
class AppQuery extends React.Component {
  /**
   * Render the component
   * @return {Object} The rendered component
   */
  render() {
    const { children, isDevice } = this.props;
    return (isDevice === deviceIsApp()
      && (
        <div>
          {children}
        </div>
      ));
  }
}

AppQuery.propTypes = {
  isDevice: PropTypes.bool,
  children: PropTypes.any.isRequired
};

AppQuery.defaultProps = {
  isDevice: false
};

export default AppQuery;
