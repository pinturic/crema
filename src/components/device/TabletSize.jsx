import React from 'react';
import PropTypes from 'prop-types';
import MediaQuery from 'react-responsive';
import DEVICE_SIZES from 'constants/deviceSizes';

/**
 * Shows its children only when the size of the screen is at least the one of a
 * mobile and at most the one of a desktop
 * @param {PropTypes.any.isRequired} children The children to show
 * @return {Object} The rendered object
 */
function TabletSize({ children }) {
  return (
    <MediaQuery
      minWidth={DEVICE_SIZES.PHONE + 1}
      maxWidth={DEVICE_SIZES.DESKTOP}
    >
      {children}
      ;
    </MediaQuery>
  );
}

TabletSize.propTypes = {
  children: PropTypes.any.isRequired
};

export default TabletSize;
