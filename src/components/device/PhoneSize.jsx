import React from 'react';
import PropTypes from 'prop-types';
import MediaQuery from 'react-responsive';
import DEVICE_SIZES from 'constants/deviceSizes';

/**
 * Shows its children only when the size of the screen is at most the one of a
 * phone
 * @param {PropTypes.any.isRequired} children The children to show
 * @return {Object} The rendered object
 */
function PhoneSize({ children }) {
  return (
    <MediaQuery maxWidth={DEVICE_SIZES.PHONE}>
      {children}
      ;
    </MediaQuery>
  );
}

PhoneSize.propTypes = {
  children: PropTypes.any.isRequired
};

export default PhoneSize;
