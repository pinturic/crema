import React from 'react';
import PropTypes from 'prop-types';
import MediaQuery from 'react-responsive';
import DEVICE_SIZES from 'constants/deviceSizes';

/**
 * Shows its children only when the size of the screen is at least the one of a
 * desktop
 * @param {PropTypes.any.isRequired} children The children to show
 * @return {Object} The rendered object
 */
function DesktopSize({ children }) {
  return (
    <MediaQuery minWidth={DEVICE_SIZES.DESKTOP + 1}>
      {children}
      ;
    </MediaQuery>
  );
}

DesktopSize.propTypes = {
  children: PropTypes.any.isRequired
};

export default DesktopSize;
