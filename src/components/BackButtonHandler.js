import React from 'react';
import PropTypes from 'prop-types';

/**
 * Handles the tap on back event. Pages should include this to react.
 * @return {Object} The BackButtonHandler object
 */
class BackButtonHandler extends React.Component {
  /**
   * Called when the component mounts
   * @return {Object} Does not return anything
   */
  componentDidMount() {
    document.addEventListener('backbutton', this.onBackKeyDown, false);
  }

  /**
   * Called when the component unmounts
   * @return {Object} Does not return anything
   */
  componentWillUnmount = () => document.removeEventListener(
    'backbutton',
    this.onBackKeyDown
  );

  /**
   * Called when pressing the back button
   * @param {Event} e The back button event
   * @return {Object} Does not return anything
   */
  onBackKeyDown = (e) => {
    e.preventDefault();
    const { onBack } = this.props;
    return onBack();
  }

  /**
   * This component does not show anything
   * @return {Object} Always false
   */
  render() {
    return false;
  }
}

BackButtonHandler.propTypes = {
  onBack: PropTypes.func.isRequired
};

export default BackButtonHandler;
