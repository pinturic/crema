import CircularProgress from '@material-ui/core/CircularProgress';
import React from 'react';
import PropTypes from 'prop-types';

/**
 * Shows a loading indicator during async operations
 * @param {Boolean} loading  true when the indicator is to be shown
 * @param {Object} children  The children components that are to be shown
 * @return {Object}      The Loading Component
 */
function Loading({ loading, children }) {
  return (
    loading
      && <CircularProgress />
    || children
  );
}

Loading.propTypes = {
  loading: PropTypes.bool,
  children: PropTypes.any.isRequired
};

export default Loading;
