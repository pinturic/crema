/**
 * Returns true if CReMa is running inside a Cordova application false if it is
 *   the browser
 * @return {Boolean} It is true if inside a Cordova application
 */
const isApp = function () {
  // provided that we are calling this after cordovas has loaded it is better
  return !!(window.cordova || window.PhoneGap || window.phonegap);
};

export default isApp;
