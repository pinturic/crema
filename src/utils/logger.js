import loglevel from 'loglevel';

/**
 * The colors defined for "TRACE" "DEBUG" "INFO" "WARN" "ERROR" "SILENT" levels
 * of error
 * @type {Array}
 */
const colors = [
  '#009999',
  '#0000FF',
  '#999900',
  '#FF7700',
  '#FF0000',
  '#770000',
];

/**
 * Returns a logger for the passed category.
 * @param  {[type]} category An optional category for the logger
 *   ('crema is the default')
 * @return {[type]}          A logging element (with log4java typical methods:
 *   error(), debug(), etc)
 */
const getLogger = (category) => {
  const cat = category || 'crema';
  const level = JSON.parse(process.env.LOGGER).levels[cat] || 'DEBUG';
  const logger = loglevel.getLogger(cat);

  if (!logger.cremaLogger) {
    const originalFactory = logger.methodFactory;
    logger.methodFactory = (methodName, logLevel, loggerName) => {
      const rawMethod = originalFactory(methodName, logLevel, loggerName);
      return function (message) {
        rawMethod(
          `%c[${
            new Date()
          } - ${cat
          } - ${
            methodName.toUpperCase()
          }] %c${
            message}`,
          `color: ${colors[logger.levels[methodName.toUpperCase()]]};`,
          'color: black;'
        );
      };
    };
    logger.cremaLogger = true;
  }

  logger.setLevel(level);
  return logger;
};

export default getLogger;
