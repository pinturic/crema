/**
 * Checks for the availability of i18n in current browser and in case of
 * need loads the i18n polyfills
 * @return {Promise} A promise resolved whrn 1i18n is ready
 */
const loadIntl = () => {
  const promise = new Promise((resolve) => {
    // check if the browser support Intl otherwise load the Intl polyfill
    // written this way so that dependencies can be statically extracted
    if (!global.Intl) {
      require.ensure([
        'intl',
        'intl/locale-data/jsonp/ar',
        'intl/locale-data/jsonp/de',
        'intl/locale-data/jsonp/en',
        'intl/locale-data/jsonp/es',
        'intl/locale-data/jsonp/fr',
        'intl/locale-data/jsonp/it',
        'intl/locale-data/jsonp/ja',
        'intl/locale-data/jsonp/pt',
        'intl/locale-data/jsonp/ru',
        'intl/locale-data/jsonp/zh'
      ], (require) => {
        require('intl');
        // this is needed for all the devices that do not have intl at all
        require('intl/locale-data/jsonp/ar');
        require('intl/locale-data/jsonp/de');
        require('intl/locale-data/jsonp/en');
        require('intl/locale-data/jsonp/es');
        require('intl/locale-data/jsonp/fr');
        require('intl/locale-data/jsonp/it');
        require('intl/locale-data/jsonp/ja');
        require('intl/locale-data/jsonp/pt');
        require('intl/locale-data/jsonp/ru');
        require('intl/locale-data/jsonp/zh');

        resolve(window.Intl);
      });
    } else {
      resolve(global.Intl);
    }
  });
  return promise;
};

export default loadIntl;
