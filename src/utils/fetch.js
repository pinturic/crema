// there is a problem in fetch library and cordova so let's use
// the the XmlHtppRequest polyfill
/* eslint-disable no-restricted-globals */
import isApp from 'utils/device';
import UAParser from 'ua-parser-js';

if (isApp() || new UAParser().getResult().browser.name === 'Edge') {
  self.fetch = null;
}

/**
 * The fetch API (native or polyfill) provided by isomorphic-fetch
 * @type {Object}
 */
const fetch = require('isomorphic-fetch');

export default fetch;
