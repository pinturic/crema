/**
 * Called when the application gos in pause
 * @param  {Object} store The application store
 * @return {Object}       Does not return anything
 */
function onPause(store) {
  const dump = JSON.stringify(store.getState());
  localStorage.setItem('reduxStore', dump);
}

export default onPause;
