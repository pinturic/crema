// ie9 console fix
import '@babel/polyfill'; // babel polyfill (Object.assign, ...)
import React from 'react';
import ReactDOM from 'react-dom';
import { createBrowserHistory, createHashHistory } from 'history';
import isApp from 'utils/device';
import { Provider } from 'react-redux';
import App from 'app/App';
import configureStore from 'app/store/configureStore';
import { Route } from 'react-router';
// load the Intl library
import loadIntl from 'utils/loadIntl';
import getLogger from 'utils/logger';
import onPause from 'utils/onPause';
import { changePage } from 'app/actions/app';
import CloseSplashScreen from 'components/CloseSplashScreen';
import { ConnectedRouter } from 'connected-react-router';
import version from '../version.json';

// in case they do not exist...
if (!window.console) window.console = {};
if (!window.console.log) window.console.log = function () { };

getLogger().info(`Started CReMa version: ${version.version}`);

/**
 * The store for the application; defines here and used for the whole app life
 * @type {Object}
 */
let store = null;

/**
 * The history component used by the react router; cordova applications do not
 * user browserHistory, since they are file based need hashHistory.
 * @type {Object}
 */
const history = !isApp() ? createBrowserHistory() : createHashHistory();

/**
 * Contains a string representation of the store status saved in local storage
 * @type {String}
 */
const initialStateStr = localStorage.getItem('reduxStore') || '{}';

/**
 * Contains a json representation of the store status saved in local storage
 * @type {Object}
 */
const initialState = JSON.parse(initialStateStr);
store = configureStore(history, initialState);

/**
 * Called to render the application
 * @return {Object} React rendering output
 */
const renderApp = function () {
  ReactDOM.render(
    (
      <div>
        <Provider store={store}>
          <ConnectedRouter history={history}>
            <div>
              <Route path="/" component={App} />
            </div>
          </ConnectedRouter>
        </Provider>
        <CloseSplashScreen />
      </div>
    ),
    document.getElementById('app')
  );
};

// set the listener for the events
document.addEventListener('pause', () => onPause(store), false);
window.addEventListener('beforeunload', () => onPause(store));

loadIntl().then(() => {
  renderApp();
  if (initialState && initialState.router && initialState.router.location) {
    const { location } = initialState.router;
    try {
      store.dispatch(changePage(location.pathname, location.query));
    } catch (e) {
      getLogger().error(`Error in setting the old page: ${e}`);
    }
  }
});
