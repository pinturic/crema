
# Cordova icons and splashscreens configuration

You can configure ./assets/android/icon.png and ./assets/ios/icon.png icons 
(as welll as the splashscreens that you find in ./assets/android and ./assets/ios
subfolders);
after modifying them you have to run npm install to create the cordova applications. 

# Cordova applications configuration

In the package.json file there is a section dedicated to the configuration for you application

~~~
"cordova": {
    "id": "com.crema.app",
    "name": "crema",
    "plugins": [
      "cordova-plugin-device",
      "cordova-plugin-console",
      "cordova-plugin-inappbrowser",
      "cordova-plugin-splashscreen",
      "cordova-plugin-whitelist"
    ]
  }
~~~

You SHOULD modify both **id** and **name** to be unique and meet your needs.
In the res folder there are the resources (icons and splashcreens that will be used in your cordova apps)

# Log 4js configuration

In LOGGER section of config files you can configure appenders for the logger. The resource page has an
example of the logger usage.