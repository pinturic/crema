[![bitHound Overall Score](https://www.bithound.io/bitbucket/pinturic/crema/badges/score.svg)](https://www.bithound.io/bitbucket/pinturic/crema)
[![bitHound Dependencies](https://www.bithound.io/bitbucket/pinturic/crema/badges/dependencies.svg)](https://www.bithound.io/bitbucket/pinturic/crema/d2f4223edec3a203961fbd7b4f528e5bf15b2921/dependencies/npm)
[![bitHound Dev Dependencies](https://www.bithound.io/bitbucket/pinturic/crema/badges/devDependencies.svg)](https://www.bithound.io/bitbucket/pinturic/crema/d2f4223edec3a203961fbd7b4f528e5bf15b2921/dependencies/npm)
[![bitHound Code](https://www.bithound.io/bitbucket/pinturic/crema/badges/code.svg)](https://www.bithound.io/bitbucket/pinturic/crema)
![ Code documentation ](./esdoc/badge.svg)


# 🍨 CReMa 

**CReMa** (**C**ordova **Re**act **Ma**terial-UI) is a base project to develop web and cordova applications all together.

| We need            | Without CReMa                       | With CReMa         |
| :----------------- |:------------------------------------|:-------------------|
| A web application  | 😀 Yes                              | 😀 Yes!            |
| and responsive     | 🙄 Well with next release...        | 😀 Yes!            |
| and Android app    | 😓 With external support maybe...   | 😀 Yes!            |
| and iOS app        | 😱 ... Help!!!!!                    | 😀 Yes!            |

![Images examples](./docs/imgs/example.gif)

# Table of contents
- [Basic system](#markdown-header-basic-system)
- [Prerequisites](#markdown-header-prerequisites)
- [Install](#markdown-header-install)
- [Easy start](#markdown-header-easy-start)
- [Add your code](#markdown-header-add-your-code)
- [Web build](#markdown-header-web-build)
- [Cordova builds](#markdown-header-cordova-builds)
    - [Cordova build](#markdown-header-cordova-build)
    - [Cordova run](#markdown-header-cordova-run)
- [Advanced topics](#markdown-header-advanced-topics)
- [Change log](#markdown-header-change-log)
- [Code documentation](#markdown-header-code-documentation)
- [What's next?](#markdown-header-whats-next)

# Basic system

It is built with these characteristics in mind:

* easy to configure and customize
* handy development and debugging
* automatic deployment of web and devices applications


These are the main components used to build CReMA:

* **React** ecosystem to build the application
	* [React Router](https://github.com/reactjs/react-router) as a routing library
	* [Redux](https://github.com/reactjs/redux) (and [React Redux](https://github.com/reactjs/react-redux)) as the state container
	* [React Responsive](https://github.com/contra/react-responsive) to build responsive components
	* [React Intl](https://github.com/yahoo/react-intl) for the internationalization
* **Babel** to use the best js feature available
* **Material-ui** to give the application a nice and light style
* **Webpack** to start the development site and package the app
* **Cordova** to package the app for Android and iOS

# Prerequisites
To work with CReMa you simply need to have installed `node` and `npm` on your platform; cordova and all the other dependencies involved in building and running the application are downloaded and installed in the local package. For this reason you don't have to install cordova yourself (nor you have to check for incompatibilities with cordova or other globally installed packages).
* to build your app for Android you need the [Android Developer Studio](https://developer.android.com/studio/index.html).
* to build your app for Ios you need [Xcode](https://developer.apple.com/xcode/). 
* to install and run the web application built with CReMa you need a web server (like Apache).

# Install
Installing CReMa is as easy as executing:
~~~
git clone https://bitbucket.org/pinturic/crema.git
cd crema
npm install
~~~

# Easy start
To start the development web browser run this:
~~~
cd crema
npm start
~~~

And then open your favourite web browser at the address http://localhost:8080. CReMa is using hot reloading so the changes made to the code are immediately reflected in the app; after any code change you simply can refresh the page to see the updates.

# Add your code
The routes used by react router can be found in `crema/src/app.jsx` file:
~~~
const AppRoutes = (
  <Router history={history}>
    <Route path="/" component={ReduxApp}>
      <IndexRedirect to="/deviceInfo" />
      <Route path="deviceInfo" component={DeviceInfo} />
      <Route path="guiSample" component={GuiSample} />
      <Route path="resources" component={Resources} />
      <Route path="fetchApi" component={FetchApi} />
    </Route>
  </Router>
);
~~~

CReMa comes with four example pages you can find in `src/pages`; each of these pages includes one example regarding CReMa features:

* device detection
* fetch remote data
* using Material UI
* including resources (as images)
* including the internationalization library

To build your code simply take advantage of the existing pages and modify them or build yours. Then include the newly created pages in `Approutes`.
There is a handy `config.js` that can be used to store your configuration data; you can find it in:

* config/android.env
* config/development.env
* config/ios.env
* config/production.env

# Web build
To build your application simply run:
~~~
cd crema
npm run build
~~~
In `output/web` you will find the html / js / images built by CReMa; these files are to be served by your favourite web server.

# Cordova builds
In the `scripts` folder there are two handy scripts to build and run your cordova application `cordovaBuild.js` and `cordovaRun.js`.
## Cordova build
Run `node cordovaBuild.js --help` to see the available options for this command.
Running the following example will build the android application in debug mode for an emulator.
~~~
node cordovaBuild.js -d -e -p android
~~~
You can call the same script with `npm`. The following command will build the ios application in debug mode for an emulator
~~~
npm run cordovaBuild -- -d -e -p ios
~~~

It is worth signaling that an Xcode project is generate in `crema/devices/ios/platforms/ios/crema.xcodeproj` so you can run the application with the aid of Xcode instead of using the command line.

## Cordova run
Run `node cordovaRun.js --help` to see the available options for this command. 
Running the following example will run the android application in debug mode for an emulator.
~~~
node cordovaRun.js -d -e -p android
~~~
You can call the same script with `npm`. The following command will run the ios application in debug mode for an emulator
~~~
npm run cordovaRun -- -d -e -p ios
~~~

## Cordova plugins handling
Run `node cordovaPlugins.js --help` to see the available options for this command.
Running the following example will list the plugins available for the android application.
~~~
node cordovaPlugins.js -p android -l
~~~
You can call the same script with `npm`. The following command will list the plugins available for the android application.
~~~
npm run cordovaPlugins -- -p ios -l
~~~

# Advanced topics

[ Advanced topics ](./docs/ADVANCED.md)

# Change log

[ Change log ](./docs/CHANGE_LOG.md)

# Code documentation

[ Code documentation ](./esdoc/index.html)

# What's next?

* Add Splashscreen configuration
* Add testing
* Add scripts documentation
* Improve the documentation
